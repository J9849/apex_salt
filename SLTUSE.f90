      SUBROUTINE SLTUSE()
!     APEX0806 Jaehak Jeong 2013
!     THIS SUBPROGRAM COMPUTES ACTUAL SALT PLANT UPTAKE FROM EACH
!     LAYER(UPTAKE = MINIMUM OF PLANT DEMAND AND SOIL SUPPLY). 
           
      USE PARM
      
      REAL:: slt_dmnd, slt_soil, rto, opt_biom_slt, up_rate
      real:: max_slt_uptake,xx,rtd
      REAL, DIMENSION(NBSL(ISA)):: ECSLT

!------------------------------------------------------------------------
!     ECSLT    :  Current soil salt concentration dS/m 
!     DM1      :  Actual amount of biomass ton/ha
!     USLT1    :  Actual amount of salt in the biomass kg/ha
!     WSLT     :  Amount of salt in the soil layer kg/ha
!     UW       :  Water uptake by plant mm
!     max_slt_uptake     :  Max salt uptake amount for the day kg/ha 
!------------------------------------------------------------------------
      IF(SUM(UW(:))<1.E-5) RETURN
      !Calculate current salt amount in EC units
      XX=0.
      DO I=1,NBSL(ISA)
         ISL=LID(I,ISA)
         IF(I>1) THEN
            DG=1000.*(Z(ISL,ISA)-Z(LID(I-1,ISA),ISA)) !mm
         ELSE
            DG = 1000.*Z(LID(1,ISA),ISA)
         END IF
         
         ECSLT(ISL)=WSLT(ISL,ISA)/6.4/(DG-PO(ISL,ISA)) !Soil salt in EC units
         IF(RD(JJK,ISA)>=Z(ISL,ISA))THEN
            rtd=DG/1000./RD(JJK,ISA)
            XX=XX+ECSLT(ISL)*rtd
         ELSE
            IF(I>1) THEN
                XZ=1000.*(RD(JJK,ISA)-Z(LID(I-1,ISA),ISA))
            ELSE
                XZ=1000.*(RD(JJK,ISA))
            ENDIF
            rtd=XZ/1000./RD(JJK,ISA)
            XX=XX+ECSLT(ISL)*rtd        !XX=mean salt concentration between soil layers in root depth dS/m
            EXIT
         END IF          
      END DO

      !Optimal salt content in the biomass, kg/ha
      !Formula came from N.Goehring's greenhouse data at U. Nevada Reno
      IF (XX<=2.) THEN
          opt_biom_slt = 45798. * XX * DM1(JJK,ISA) / 1.E6 !mg/kg * kg/ha -> kg/ha
      ELSE
          opt_biom_slt = (2612.7 * XX + 86370.) * DM1(JJK,ISA) / 1.E6
      END IF

      !Max daily salt uptake kg/ha
      max_slt_uptake=0.
      DO J=1,LRD(ISA)
         max_slt_uptake = max_slt_uptake + WSLT(J,ISA) * UW(J) / (SWST(J,ISA)+.001) !kg/ha
      END DO

      !Salt uptake demand: the difference between optimal and actual salt content in biomass kg/ha
      slt_dmnd = max(0.,opt_biom_slt - USLT1(JJK,ISA))
      IF (slt_dmnd>max_slt_uptake) THEN
          slt_dmnd = max_slt_uptake    !daily salt uptake should not exceed daily max amount
      END IF
            
      DO J=1,LRD(ISA)
         !Salt uptake from soils in the root depth
         up_rate = slt_dmnd * UW(J)/SUM(UW(:))*(1.-REG(JJK,ISA)) !plant uptake is reduced by stress factor
         
         USLT1(JJK,ISA) = USLT1(JJK,ISA) + up_rate         
         WSLT(J,ISA) = WSLT(J,ISA) - up_rate
      END DO
 	  SMMC(18,JJK,MO,ISA)=USLT1(JJK,ISA)
      
      write(5127679232,'(2I5,15e12.4)') iyr,Ida,DM1(JJK,ISA),USLT1(JJK,ISA),XX,max_slt_uptake,opt_biom_slt,slt_dmnd,(1.-REG(JJK,ISA)),(ecslt(lid(i,isa)), i=1,4)
         
      RETURN
      END
      
           
